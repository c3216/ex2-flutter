import 'package:flutter/material.dart';

final _icons = <String, IconData> {
  'access_alarms': Icons.access_alarm,
  'add_alert_outlined': Icons.add_alert_outlined
};

IconData getIcon(String iconName) {
  return _icons[iconName] ?? Icons.access_time;
}