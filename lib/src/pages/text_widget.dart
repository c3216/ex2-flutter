import 'package:flutter/material.dart';

class TextWidget extends StatelessWidget {
  String name = '';

  TextWidget({this.name = ''});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 200.0,
      color: Colors.blue,
      child: Text('$name'),
    );
  }
}