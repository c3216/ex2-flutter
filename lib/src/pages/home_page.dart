import 'package:ex2witgets/src/pages/text_widget.dart';
import 'package:ex2witgets/src/widgets/button_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final TextStyle _estilos = TextStyle(color: Colors.green[900]);
  final EdgeInsets _paddingText = EdgeInsets.only(left: 20.0, top: 100.0);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('home page'),
      ),
      body: Center(
          // Column Vertical, Row horizontal
          child: Column(
        children: <Widget>[
          // TextWidget(name: 'Hola'),
          // TextWidget(name: 'Adios'),
          // TextWidget(name: 'Hola 2'),

          Padding(
            padding: _paddingText,
            child: const Text(
              'Es un Texto',
              style: TextStyle(
                  color: Color(0xFFFA1EC8),
                  fontSize: 20.00,
                  fontWeight: FontWeight.bold),
            ),
          ),
          Padding(
            padding: _paddingText,
            child: TextField(
              decoration:
                  const InputDecoration(labelText: 'Aqui se pone el telefono'),
              keyboardType: TextInputType.number,
              style: _estilos,
              inputFormatters: <TextInputFormatter>[
                FilteringTextInputFormatter.digitsOnly
              ],
              onChanged: (data) {},
            ),
          ),
          const SizedBox(
            height: 30.00,
          ),
          // Antes
          ButtonWidget(
            btnText: 'Hola a todos',
            onpressed: () {
              Fluttertoast.showToast(
                msg: 'Click button',
                toastLength: Toast.LENGTH_LONG,
                gravity: ToastGravity.CENTER,
                backgroundColor: Colors.red
              );
            }
          )
        ],
      )),
    );
  }
}
