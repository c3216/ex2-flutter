import 'package:ex2witgets/src/providers/food_provider.dart';
import 'package:ex2witgets/src/utils/icon_string_util.dart';
import 'package:ex2witgets/src/widgets/list_item_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ListPage extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('List Page'),
      ),
      body: _lista()
    );
  }

   Widget _lista() {
     return FutureBuilder(
       future: foodProvider.cargarData(),
       initialData: [],
       builder: (context, AsyncSnapshot<List<dynamic>> snapshot) {
        return ListView(
          children: _crearItemsList(snapshot.data ?? []),
        );
       },
     );
   }

  List<Widget> _crearItemsList(List data) {
    return data.map((opt) {
      return Column(
        children: [
          ListItemWidget(
            title: opt['title'],
            leading: getIcon(opt['icon']),
            ontap: () {},
          ),
          SizedBox(
            height: 10,
          )
        ],
      );
    }).toList();
  }

 

  

}