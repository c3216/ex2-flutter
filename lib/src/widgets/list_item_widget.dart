import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ListItemWidget extends StatelessWidget {
  final String title;
  final IconData? leading;
  final Function ontap;

  ListItemWidget({required this.title, required this.ontap, this.leading});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(title, style: TextStyle(fontSize: 15, color: Colors.white)),
      subtitle: Text(title, style: TextStyle(fontSize: 5, color: Colors.white)),
      leading: Icon(leading, color: Colors.white),
      trailing: Icon(Icons.delete, color: Colors.white),
      tileColor: Colors.black,
      onTap: () {
        ontap();
      },
    );
  }
}