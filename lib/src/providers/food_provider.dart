
import 'dart:convert';
import 'package:flutter/services.dart';

class _FoodProvider {
  List<dynamic> options = [];
  
  _FoodProvider() {
    cargarData();
  }

  Future<List<dynamic>> cargarData() async {
    final res = await rootBundle.loadString('data/food_opt.json');
    options = json.decode(res);
    return options;
  }
}

final foodProvider = new _FoodProvider();